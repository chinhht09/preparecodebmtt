
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.security.MessageDigest;
import java.security.spec.KeySpec;
import java.util.Base64;
import javax.crypto.Cipher;
import javax.crypto.CipherInputStream;
import javax.crypto.CipherOutputStream;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.DESKeySpec;
import javax.crypto.spec.DESedeKeySpec;
import sun.misc.BASE64Decoder;
import sun.misc.BASE64Encoder;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Chinh
 */
public class allFunctions {
    //để gọi class, nhớ khởi tạo constructor trong frame và để public với các hàm cần gọi
    public allFunctions() {
    
    }
   // tạo hàm mã hóa Ceasar
   // Input: một chuỗi đầu vào và một số nguyên k từ 0 -> 25
   // Output: một chuỗi mới đã được mã hóa
    // Ta cần một hàm mã hóa từng ký tự để thực hiện chung cho cả bước Encrypt và Dencrypt
    // hàm dưới đây sẽ thực hiện mã hóa từng ký tự hay dịch ký tự theo số nguyên key
    private char mahoa(char c, int k) {
        if(!Character.isLetter(c)) return c;
        return (char) ((((Character.toUpperCase(c) - 'A') + k) %26 + 26) %26 + 'A');
    }
    // vậy quá trình mã hóa Ceasar sẽ tiến hành mã hóa trên từng ký tự trong chuỗi
    public String EncryptCeasar(String plainText, int key)
    {
        String result = "";
        for (int i = 0; i < plainText.length(); i++) {
            char ch = mahoa(plainText.charAt(i), key);
            result += ch;
        }
        return result;
    }
    // tương tự với quá trình giải mã, nhưng để giải mã, ta tiến hành mã hóa lùi bằng cách để khóa key = -key
    public String DencryptCeasar(String plainText, int key)
    {
        String result = "";
        for (int i = 0; i < plainText.length(); i++) {
            char ch = mahoa(plainText.charAt(i), -key);
            result += ch;
        }
        return result;
    }
    
    //hàm mã hóa và giải mã Vigenere
    //đây là bản nâng cấp của Ceasar khi khóa không chỉ là 1 số nguyên duy nhất
    //khóa bây giờ phức tạp hơn, là tập hợp của nhiều khóa con(nhiều số nguyên)
    //key bây giờ có thể nhập vào một chuỗi và được so vào bảng alpha để so vị trí
    public String EncryptVigenere(String plainText, String key)
    {
        int table[][] = new int[26][26];
        for (int i = 0; i < 26; i++) {
            for (int j = 0; j < 26; j++) {
                table[i][j] = (i + j) % 26;
            }
        }
        String result = "";//chuỗi kết quả sau mã hóa
        int keyIndex = 0;//chỉ số khóa
        for (int i = 0; i < plainText.length(); i++) {
            int value;//lưu mã ascii của kí tự đã mã hóa
            char kChar = key.charAt(keyIndex);//lấy kí tự của khóa tại vị trí keyindex
            int ch2 = ((int) kChar) - 65;//lấy mã ascii của key
            char ch = plainText.charAt(i);
//            kiểm tra kí tự đặc biệt và dấu cách
            if (!(65 <= (int) ch && (int) ch <= 90) && !(97 <= (int) ch && (int) ch <= 122)) {
                result += ch;
                keyIndex--;
            }
            if (Character.isUpperCase(ch)) {
                int ch1 = ((int) ch) - 65;
                value = table[ch1][ch2];
                result += (char) (value + 65);
                //JOptionPane.showMessageDialog(null, ch1 + " "+ value +" "+ result);
            } else if (Character.isLowerCase(ch)) {
                int ch1 = ((int) ch) - 97;
                value = table[ch1][ch2];
                result += (char) (value + 97);
                //JOptionPane.showMessageDialog(null, ch1 + " "+ value +" "+ result);
            }
            keyIndex++;
            if (keyIndex >= key.length()) {
                keyIndex = 0;
            }
        }
        return result;
    }
    public String DencryptVigenere(String cipherText, String key)
    {
        int table[][] = new int[26][26];
        for (int i = 0; i < 26; i++) {
            for (int j = 0; j < 26; j++) {
                table[i][j] = (i + j) % 26;
            }
        }
        String result = "";//chuỗi kết quả sau mã hóa
        int keyIndex = 0;//chỉ số khóa

        for (int i = 0; i < cipherText.length(); i++) {
            char kChar = key.charAt(keyIndex);//lấy kí tự của khóa tại vị trí keyindex
            int ch2 = ((int) kChar) - 65;//lấy mã ascii của key
            //JOptionPane.showMessageDialog(null, ch2);
            char ch = cipherText.charAt(i);
            //kiểm tra kí tự đặc biệt và dấu cách
            if (!(65 <= (int) ch && (int) ch <= 90) && !(97 <= (int) ch && (int) ch <= 122)) {
                result += ch;
                keyIndex--;
            }
            for (int j = 0; j < 26; j++) {
                if (Character.isUpperCase(ch)) {
                    if (table[ch2][j] == (int) ch - 65) {
                        char ch1 = (char) (j + 65);
                        result += ch1;
                    }
                    //JOptionPane.showMessageDialog(null, ch1 + " "+ value +" "+ result);
                } else if (Character.isLowerCase(ch)) {
                     if (table[ch2][j] == (int) ch - 97) {
                        char ch1 = (char) (j + 97);
                        result += ch1;
                     }
                }
            }
            keyIndex++;
            if (keyIndex >= key.length()) {
                keyIndex = 0;
            }
        }
        return result;
    }

    //hàm mã hóa và giải mã Rail Fence
    // còn được gọi là mã zig zag, là 1 hình thức của mã chuyển vị
    //Thông điệp được viết lần lượt từ trái qua phải trên các cột (rail) của một
    //hàng dào tưởng tượng theo đường chéo từ trên xuống dưới.
    // Theo đường chéo từ dưới lên khi đạt tới cột thấp nhất.
    // Và khi đạt tới cột cao nhất, lại viết theo đường chéo từ trên xuống. Cứ lặp
    //đi lặp lại như thế nào cho đến khi viết hết toàn bộ nội dung của thông điệp.
    public String EncryptRailFence(String plainText, int key)
    {
        int i, j, r, s;
        String result = "";
        r = key;//r là khóa củng là số hàng của màng
        //tính số cột của mảng
        if (plainText.length() % r == 0) {
            s = plainText.length() / r;
        } else {
            s = (plainText.length() / r) + 1;
        }
        //s = 2 * (plainText.length() / r); //tính chiều dài của mảng
        char ct[][] = new char[r][s];

        for (i = 0; i < r; i++) {
            int t = i;
            for (j = 0; j < s; j++) {
                ct[i][j] = plainText.charAt(t);
                t = t + r;
                if (t > plainText.length() - 1) {
                    break;
                }
            }
        }
//      lấy chuỗi trong mảng ra
        for (i = 0; i < r; i++) {
            for (j = 0; j < s; j++) {
                char d = ct[i][j];
                if (d == '\0') {
                    continue;
                } else {
                    result = result + ct[i][j];
                }
            }
        }
        return result;
    }
    
    public String DencryptRailFence(String cipherText, int key)
    {
        int r = key;//khóa củng là số dòng
        String result = "";
        int s;//số cột của mảng
        if (cipherText.length() % r == 0) {
            s = cipherText.length() / r;
        } else {
            s = (cipherText.length() / r) + 1;
        }

        for (int i = 0; i < s; i++) {
            int k = i;
            while (k < cipherText.length()) {
                result += cipherText.charAt(k);
                k+=(r+1);
            }
        }
        return result;
    }
    
    
    //hàm giải mã và mã hóa của DES
    // lưu ý là key của DES phải trên 8 ký tự
    private int mode;
    //đảm nhiệm việc lưu kết quả sau khi Encrypt hoặc Dencrypt vào file mới os
    private static void doCopy(InputStream is, OutputStream os) throws IOException{
        byte[] bytes = new byte[64];
        int numBytes;
        while((numBytes = is.read(bytes)) != -1){
            os.write(bytes, 0, numBytes);
        }
        os.flush();
        os.close();
        is.close();
    }
    
    private static void encryptOrDecypt(String key, int mode, InputStream is, OutputStream os) throws Throwable{
        DESKeySpec dks = new DESKeySpec(key.getBytes());
        SecretKeyFactory skf = SecretKeyFactory.getInstance("DES");
        SecretKey desKey = skf.generateSecret(dks);
        Cipher cipher = Cipher.getInstance("DES");
        
        if(mode == Cipher.ENCRYPT_MODE){
            cipher.init(Cipher.ENCRYPT_MODE, desKey);
            CipherInputStream cis = new CipherInputStream(is, cipher);
            doCopy(cis, os);
        }else if(mode == Cipher.DECRYPT_MODE){
            cipher.init(Cipher.DECRYPT_MODE, desKey);
            CipherOutputStream cos = new CipherOutputStream(os, cipher);
            doCopy(is, cos);
        }
    }
    
    private static void encryptDes(String key, InputStream is, OutputStream os) throws Throwable{
        encryptOrDecypt(key, Cipher.ENCRYPT_MODE, is, os);
    }
    
    private static void dencryptDes(String key, InputStream is, OutputStream os) throws Throwable{
        encryptOrDecypt(key, Cipher.DECRYPT_MODE, is, os);
    }
    
    public String EncryptDES(String plainText, String key)
    {
        String result = "_";
        try
        {
            WriteFile("plainTextDES.txt", plainText);
            FileInputStream fis = new FileInputStream("plainTextDES.txt");
            FileOutputStream fos = new FileOutputStream("EnDes.txt");
            encryptDes(key, fis, fos);
            result = ReadFile("EnDes.txt");
        }
        catch (Throwable e)
        {
            e.printStackTrace();
        }
        return result;
    }
    
    public String DencryptDES(String cipherText, String key)
    {
        String result = "_";
        try
        {
            FileInputStream fis = new FileInputStream("EnDes.txt");
            FileOutputStream fos = new FileOutputStream("DeDes.txt");
            dencryptDes(key, fis, fos);
            result = ReadFile("DeDes.txt");
        }
        catch (Throwable e)
        {
            e.printStackTrace();
        }
        return result;
    }
    
    //Triple DES
    private static final String UNICODE_FORMAT = "UTF8";
    public static final String DESEDE_ENCRYPTION_CHEME = "DESede";
    private KeySpec myKeySpec;
    private SecretKeyFactory mySecretKeyFactory;
    private Cipher cipher;
    byte[] keyAsBytes;
    private String myEncryptionKey;
    private String myEncryptionScheme;
    SecretKey key;
    
    private String encrypt3Des(String unencryptedString){
        String encryptedString = null;
        try{
            cipher.init(Cipher.ENCRYPT_MODE, key);
            byte[] plainText = unencryptedString.getBytes(UNICODE_FORMAT);
            byte[] encryptedText = cipher.doFinal(plainText);
            BASE64Encoder base64coder = new BASE64Encoder();
            encryptedString = base64coder.encode(encryptedText);
        }catch(Exception e){
            e.printStackTrace();
        }
        return encryptedString;
    }
    
    private String dencrypt3Des(String encryptedString){
        String decryptedText  = null;
        try{
            cipher.init(Cipher.DECRYPT_MODE, key);
            BASE64Decoder base64decoder = new BASE64Decoder();
            byte[] encryptedText = base64decoder.decodeBuffer(encryptedString);
            byte[] plainText = cipher.doFinal(encryptedText);
            String a =  new String(plainText);
            System.out.println("chuoi plaintex :" + a);
            decryptedText = a;
        }catch(Exception e){
            e.printStackTrace();
        }
        return decryptedText;
    }
    
    public String EncryptTripleDES(String plainText, String keyText)
    {
        String encrypted = "_";
        try{
            myEncryptionKey = keyText;
            myEncryptionScheme = DESEDE_ENCRYPTION_CHEME;
            keyAsBytes = myEncryptionKey.getBytes(UNICODE_FORMAT);
            myKeySpec = new DESedeKeySpec(keyAsBytes);
            mySecretKeyFactory = SecretKeyFactory.getInstance(myEncryptionScheme);
            cipher = Cipher.getInstance(myEncryptionScheme);
            key = mySecretKeyFactory.generateSecret(myKeySpec);
            System.out.println("khoa ma hoa k: " + " " + key);
            encrypted = encrypt3Des(plainText);
            System.out.println("Encrypted Valud: " + encrypted);
        }catch(Exception ex){}
        return encrypted;
    }
    
    public String DencryptTripleDES(String cipherText, String keyText)
    {
        String decrypt = "_";
        try{
            myEncryptionKey = keyText;
            myEncryptionScheme = DESEDE_ENCRYPTION_CHEME;
            keyAsBytes = myEncryptionKey.getBytes(UNICODE_FORMAT);
            myKeySpec = new DESedeKeySpec(keyAsBytes);
            mySecretKeyFactory = SecretKeyFactory.getInstance(myEncryptionScheme);
            cipher = Cipher.getInstance(myEncryptionScheme);
            key = mySecretKeyFactory.generateSecret(myKeySpec);
            System.out.println("khoa ma hoa k: " + " " + key);
            decrypt = dencrypt3Des(cipherText);
            System.out.println("Encrypted Valud: " + decrypt);
        }catch(Exception ex){}
        return decrypt;
    }
    
    //hàm mã hóa và giải mã của AES
    //đặc điểm cần nhớ là AES khóa tự sinh :)), vậy sau khi sinh khóa ta sẽ lưu
    //lại ở 1 file và đọc lại khi giải mã
    //Các phương thức bảo mật ở trên có truyền tham số key, ta có thể tùy biến nhập
    //tay hoặc mở file lưu file key. Trường hợp này ta gắn sẵn vào hàm
    //SecretKey secretKey;
    //byte[] byteCipherText;
    public String EncryptAES(String plainText)
    {
        String result = "_";
        try 
        {
            //tạo khóa cho toàn bộ quá trình của AES
            KeyGenerator keyGen = KeyGenerator.getInstance("AES");
            keyGen.init(128);
            SecretKey secretKey = keyGen.generateKey();
            //sau khi sinh khóa ta tiến hành lưu khóa
            FileOutputStream os = new FileOutputStream("keyAES.dat");
            ObjectOutputStream oos = new ObjectOutputStream(os);
            oos.writeObject(secretKey);
            //Tiến hành mã hóa
            System.out.println("Sinh khoa: " + secretKey);
            Cipher aesCipher = Cipher.getInstance("AES");
            aesCipher.init(Cipher.ENCRYPT_MODE, secretKey);
            String strData = plainText;
            byte[] byteDataToEncypt = strData.getBytes();
            byte[] byteCipherText = aesCipher.doFinal(byteDataToEncypt);
            String strCipherText = new BASE64Encoder().encode(byteCipherText);
            System.out.println("Cipher text generated using AES is" + strCipherText);
            result = strCipherText;
            WriteFile("cipherAES.txt", strCipherText);
        } catch (Exception e) {}
        return result;
    }
    
    public String DencryptAES()
    {
        String result ="_";
        try{
            //mở khóa secretKey đã được lưu từ lúc mã hóa ở file keyAES.dat
            FileInputStream fis = new FileInputStream("keyAES.dat");
            ObjectInputStream ois = new ObjectInputStream(fis);
            SecretKey secretKey = (SecretKey)ois.readObject();
            //đọc cipherText đã được lưu sau quá trình mã hóa ở file cipherAES.txt
            //sau khi đọc cần convert từ String thành byte[]
            String cipherText = ReadFile("cipherAES.txt");
            byte[] byteCipherText = new BASE64Decoder().decodeBuffer(cipherText);
            Cipher aesCipher = Cipher.getInstance("AES");
            aesCipher.init(Cipher.DECRYPT_MODE, secretKey, aesCipher.getParameters());
            byte[] byteDecryptedText = aesCipher.doFinal(byteCipherText);
            String strDecryptedText = new String(byteDecryptedText);
            System.out.println("Decrypted Text message is " + strDecryptedText);
            result = strDecryptedText;
        }catch(Exception ex){}
        return result;
    }
    
    //hàm băm MD5
    public String HashMD5(String plainText)
    {
        String hash = "";
        try 
        {
            MessageDigest md = MessageDigest.getInstance("MD5");
            md.update(plainText.getBytes());
            byte[] byteData = md.digest();
            StringBuffer sb = new StringBuffer();
            for (int i = 0; i < byteData.length; i++) {
                sb.append(Integer.toString((byteData[i] & 0xff) + 0x100, 16).substring(1));
            }
            hash = sb.toString();
        } catch (Exception ex) {
            System.out.println("loi : " + ex);
        }
        return hash;
        
    }
    
    public String SHA(String plainText){
        String bam = "";
        try 
        {
            MessageDigest md = MessageDigest.getInstance("SHA-256");
            md.update(plainText.getBytes());
            byte byteData[] = md.digest();
            StringBuffer sb = new StringBuffer();
            for(int i = 0; i<byteData.length;i++){
                sb.append(Integer.toString((byteData[i] & 0xff) + 0x100, 16).substring(1));
            }
            bam = sb.toString();
        } catch (Exception ex) {
            System.out.println("loi : " + ex);
        }
        return bam;
    }
    //tạo hàm lưu file và đọc file cho toàn bộ chương trình
    // i.e path = "Des.txt"
    public void WriteFile(String path, String text)
    {
        try 
        {
            BufferedWriter bw = null;
            String fileName = path;
            String s = text;
            bw = new BufferedWriter(new FileWriter(fileName));
            bw.write(s);
            bw.close();
            System.out.println("ghi file thanh cong");
        } 
        catch (Exception ex)
        {
            System.out.println("loi roi ban yeu " + ex);
        }
    }
    
    public String ReadFile(String path)
    {
        String chuoi = "NOT READ";
        try
        {
            BufferedReader br = null;
            String fileName = path;
            br = new BufferedReader(new FileReader(fileName));
            StringBuffer sb = new StringBuffer();
            
            System.out.println("mo file roi nha");
            char[] ca = new char[5];
            while(br.ready()){
                int len = br.read(ca);
                sb.append(ca, 0, len);
            }
            br.close();
            chuoi = sb.toString();
        }
        catch (Exception ex)
        {
            System.out.println("loi roi ban yeu " + ex);
        }
        return chuoi;
    }
}
