/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BaiThiThu1;

import javax.swing.JOptionPane;

/**
 *
 * @author Chinh
 */
public class jDencrypt extends javax.swing.JFrame {

    /**
     * Creates new form jDencrypt
     */
    allFunctions func = new allFunctions();
    public jDencrypt() {
        initComponents();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        txt_e3 = new javax.swing.JTextField();
        btnMoaHoaE3 = new javax.swing.JButton();
        jLabel3 = new javax.swing.JLabel();
        txt_k3 = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        txt_d3 = new javax.swing.JTextField();
        GiaiMaE3 = new javax.swing.JButton();
        jLabel5 = new javax.swing.JLabel();
        txt_m3 = new javax.swing.JTextField();
        jLabel6 = new javax.swing.JLabel();
        txt_h2 = new javax.swing.JTextField();
        jLabel7 = new javax.swing.JLabel();
        txt_n1 = new javax.swing.JTextField();
        jLabel8 = new javax.swing.JLabel();
        txt_m2 = new javax.swing.JTextField();
        jLabel9 = new javax.swing.JLabel();
        txt_e1 = new javax.swing.JTextField();
        jLabel10 = new javax.swing.JLabel();
        txt_m1 = new javax.swing.JTextField();
        jLabel11 = new javax.swing.JLabel();
        txt_k1 = new javax.swing.JTextField();
        jLabel12 = new javax.swing.JLabel();
        txt_h2h = new javax.swing.JTextField();
        btn_d3 = new javax.swing.JButton();
        btn_n1 = new javax.swing.JButton();
        btn_e1 = new javax.swing.JButton();
        btn_bamchuoi = new javax.swing.JButton();
        btnKiemTraToanVen = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel1.setText("Form Dencrypt");

        jLabel2.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel2.setText("Cipher (E3):");

        btnMoaHoaE3.setText("Mở File mã hóa E3");
        btnMoaHoaE3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnMoaHoaE3ActionPerformed(evt);
            }
        });

        jLabel3.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel3.setText("Key Encrypt (K3):");

        jLabel4.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel4.setText("Dencrypt (D3):");

        GiaiMaE3.setText("Giải mã E3");
        GiaiMaE3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                GiaiMaE3ActionPerformed(evt);
            }
        });

        jLabel5.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel5.setText("Message (M3)");

        jLabel6.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel6.setText("Message (H2)");

        jLabel7.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel7.setText("Message (N1)");

        jLabel8.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel8.setText("Message (M2)");

        jLabel9.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel9.setText("Message (E1)");

        txt_e1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_e1ActionPerformed(evt);
            }
        });

        jLabel10.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel10.setText("Message (M1)");

        jLabel11.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel11.setText("Key Encrypt (K1)");

        jLabel12.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel12.setText("Hash (H2)");

        btn_d3.setText("Tách chuỗi D3 gồm: M3 + H2 + N1");
        btn_d3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_d3ActionPerformed(evt);
            }
        });

        btn_n1.setText("Tách chuỗi N1 gồm: M2 + E1");
        btn_n1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_n1ActionPerformed(evt);
            }
        });

        btn_e1.setText("Giải Mã E1");
        btn_e1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_e1ActionPerformed(evt);
            }
        });

        btn_bamchuoi.setText("Băm Chuỗi N1");
        btn_bamchuoi.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_bamchuoiActionPerformed(evt);
            }
        });

        btnKiemTraToanVen.setText("Kiểm tra toàn vẹn");
        btnKiemTraToanVen.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnKiemTraToanVenActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(222, 222, 222)
                        .addComponent(jLabel1))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(27, 27, 27)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jLabel4)
                                .addGap(45, 45, 45)
                                .addComponent(txt_d3, javax.swing.GroupLayout.PREFERRED_SIZE, 266, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jLabel3)
                                .addGap(45, 45, 45)
                                .addComponent(txt_k3, javax.swing.GroupLayout.PREFERRED_SIZE, 266, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jLabel2)
                                .addGap(45, 45, 45)
                                .addComponent(txt_e3, javax.swing.GroupLayout.PREFERRED_SIZE, 266, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jLabel5)
                                .addGap(45, 45, 45)
                                .addComponent(txt_m3, javax.swing.GroupLayout.PREFERRED_SIZE, 266, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jLabel6)
                                .addGap(45, 45, 45)
                                .addComponent(txt_h2, javax.swing.GroupLayout.PREFERRED_SIZE, 266, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jLabel7)
                                .addGap(45, 45, 45)
                                .addComponent(txt_n1, javax.swing.GroupLayout.PREFERRED_SIZE, 266, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jLabel8)
                                .addGap(45, 45, 45)
                                .addComponent(txt_m2, javax.swing.GroupLayout.PREFERRED_SIZE, 266, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(jLabel12)
                                    .addComponent(jLabel9)
                                    .addComponent(jLabel10))
                                .addGap(45, 45, 45)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                        .addComponent(txt_e1, javax.swing.GroupLayout.PREFERRED_SIZE, 159, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(31, 31, 31)
                                        .addComponent(jLabel11))
                                    .addComponent(txt_h2h, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 266, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(txt_m1))))
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(btnMoaHoaE3))
                            .addGroup(layout.createSequentialGroup()
                                .addGap(26, 26, 26)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(btn_d3)
                                    .addComponent(GiaiMaE3)
                                    .addComponent(txt_k1, javax.swing.GroupLayout.PREFERRED_SIZE, 136, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(btn_n1, javax.swing.GroupLayout.PREFERRED_SIZE, 185, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(btn_e1)
                                    .addComponent(btn_bamchuoi))))))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addComponent(btnKiemTraToanVen)
                .addGap(280, 280, 280))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1)
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(txt_e3, javax.swing.GroupLayout.PREFERRED_SIZE, 29, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnMoaHoaE3))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(txt_k3, javax.swing.GroupLayout.PREFERRED_SIZE, 29, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel4)
                    .addComponent(txt_d3, javax.swing.GroupLayout.PREFERRED_SIZE, 29, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(GiaiMaE3))
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel5)
                            .addComponent(txt_m3, javax.swing.GroupLayout.PREFERRED_SIZE, 29, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(30, 30, 30)
                        .addComponent(btn_d3)))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel6)
                    .addComponent(txt_h2, javax.swing.GroupLayout.PREFERRED_SIZE, 29, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel7)
                    .addComponent(txt_n1, javax.swing.GroupLayout.PREFERRED_SIZE, 29, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btn_n1))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel8)
                    .addComponent(txt_m2, javax.swing.GroupLayout.PREFERRED_SIZE, 29, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel9)
                    .addComponent(txt_e1, javax.swing.GroupLayout.PREFERRED_SIZE, 29, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel11)
                    .addComponent(txt_k1, javax.swing.GroupLayout.PREFERRED_SIZE, 29, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel10)
                    .addComponent(txt_m1, javax.swing.GroupLayout.PREFERRED_SIZE, 29, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btn_e1))
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel12)
                            .addComponent(txt_h2h, javax.swing.GroupLayout.PREFERRED_SIZE, 29, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(31, 31, 31)
                        .addComponent(btn_bamchuoi)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 45, Short.MAX_VALUE)
                .addComponent(btnKiemTraToanVen)
                .addGap(28, 28, 28))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void txt_e1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_e1ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txt_e1ActionPerformed

    private void btnMoaHoaE3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnMoaHoaE3ActionPerformed
        // TODO add your handling code here:
        String e3 = func.ReadFile("cipherAES.txt");
        String k3 = func.ReadFile("keyAES.dat");
        txt_e3.setText(e3);
        txt_k3.setText(k3);
    }//GEN-LAST:event_btnMoaHoaE3ActionPerformed

    private void GiaiMaE3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_GiaiMaE3ActionPerformed
        // TODO add your handling code here:
        String enText = func.DencryptAES();
        txt_d3.setText(enText);
    }//GEN-LAST:event_GiaiMaE3ActionPerformed

    private void btn_d3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_d3ActionPerformed
        // TODO add your handling code here:
        String m3 = func.ReadFile("m3.txt");
        String h2 = func.ReadFile("h2.txt");
        String n1 = func.ReadFile("n1.txt");
        txt_m3.setText(m3);
        txt_h2.setText(h2);
        txt_n1.setText(n1);
    }//GEN-LAST:event_btn_d3ActionPerformed

    private void btn_n1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_n1ActionPerformed
        // TODO add your handling code here:
        String m2 = func.ReadFile("m2.txt");
        String e1 = func.ReadFile("e1.txt");
        txt_m2.setText(m2);
        txt_e1.setText(e1);
    }//GEN-LAST:event_btn_n1ActionPerformed

    private void btn_e1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_e1ActionPerformed
        // TODO add your handling code here:
        
        int key = Integer.valueOf(txt_k1.getText());
        String enText = func.DencryptCeasar(txt_e1.getText(), key);
        txt_m1.setText(enText);
    }//GEN-LAST:event_btn_e1ActionPerformed

    private void btn_bamchuoiActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_bamchuoiActionPerformed
        // TODO add your handling code here:
        String plainText = txt_n1.getText();
        String h2 = func.HashMD5(plainText);
        txt_h2h.setText(h2);
    }//GEN-LAST:event_btn_bamchuoiActionPerformed

    private void btnKiemTraToanVenActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnKiemTraToanVenActionPerformed
        // TODO add your handling code here:
        String hexString = txt_h2.getText();
        String chuoi = txt_h2h.getText();
        Boolean k = hexString.toString().equals(chuoi);
         if(k==true){
         JOptionPane.showMessageDialog(null, "Toàn vẹn");}
         else{
             JOptionPane.showMessageDialog(null, "Không toàn vẹn");
         }
    }//GEN-LAST:event_btnKiemTraToanVenActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(jDencrypt.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(jDencrypt.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(jDencrypt.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(jDencrypt.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new jDencrypt().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton GiaiMaE3;
    private javax.swing.JButton btnKiemTraToanVen;
    private javax.swing.JButton btnMoaHoaE3;
    private javax.swing.JButton btn_bamchuoi;
    private javax.swing.JButton btn_d3;
    private javax.swing.JButton btn_e1;
    private javax.swing.JButton btn_n1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JTextField txt_d3;
    private javax.swing.JTextField txt_e1;
    private javax.swing.JTextField txt_e3;
    private javax.swing.JTextField txt_h2;
    private javax.swing.JTextField txt_h2h;
    private javax.swing.JTextField txt_k1;
    private javax.swing.JTextField txt_k3;
    private javax.swing.JTextField txt_m1;
    private javax.swing.JTextField txt_m2;
    private javax.swing.JTextField txt_m3;
    private javax.swing.JTextField txt_n1;
    // End of variables declaration//GEN-END:variables
}
